<?php

function ft_is_sort($tableau)// la variable en argument est un prototype.
{
    $tab = sort($tableau); // on créer une variable triée avec sort() pour l'utiliser comme élément de comparaison.

    if ($tableau == $tab) {
        return true;
    } else {
        return false;
    }
}

// $tab2 = $tab;

// return $tableau == $tab;
// return $tableau == $tab ? true : false;
