<?php

/*
________
CONSIGNE
Une page après l'autre, chercher tous les liens de la page.
Les liens doivent êtres convertis en majuscules. Le script modifier doit être enregistré dans un nouveau fichier.
_____________
DEVELOPPEMENT
Ouvrir le fichier page.html.
Repérer les liens dans la page.
Convertir les minuscules de chaque lien en majuscules.
Enregistrer ces modifications dans un nouveau fichier new_page.html.
________________
COMMANDE DE TEST
/srv/src/phpLesBases/ex15> php loupe.php page.html > new_page.html
*/

/*
$opn_page = file_get_contents($argv[1]);
// echo $opn_page;

preg_match_all("/title=\".*\"/", $opn_page, $match);

//print_r($match[0]);

foreach ($match[0] as $value) {
    $liens = strtoupper($value);
    $opn_page = str_replace($value, $liens, $opn_page);
}

echo $opn_page;
*/

$opn_page = file_get_contents($argv[1]);

$transform = preg_replace_callback('/title="(.*)"/',
             function ($lien_maj) {
                 return 'title=' . '"' . strtoupper($lien_maj[1]) . '"';
             }, $opn_page);

$transform = preg_replace_callback("/(href=.*)(\>)(.*)(\<.*\>)?(\<)(\/[a])/",
             function ($lien_maj2) {
                 return $lien_maj2[1] . '>' . strtoupper($lien_maj2[3]) . '</a';
             }, $transform);

$transform = preg_replace_callback("/(href=.*)(\>)(.*)\<[img]/",
            function ($lien_maj3) {
                return $lien_maj3[1] . '>' . strtoupper($lien_maj3[3]) . '<i';
            }, $transform);

echo $transform;
