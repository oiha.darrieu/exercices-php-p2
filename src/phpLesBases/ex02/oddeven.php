<?php

while (3 == 3) {
    echo 'Entrez un nombre: ';
    $input = rtrim(fgets(STDIN));

    if (is_numeric($input)) {
        if ($input % 2 == 0) {
            echo 'Le chiffre ' . $input . " est Pair\n";
        } elseif ($input % 2 != 0) {
            echo 'Le chiffre ' . $input . " est Impair\n";
        }
    } else {
        echo "'$input'" . " n'est pas un chiffre\n";
    }
}
