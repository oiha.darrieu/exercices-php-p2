<?php

const INC_PAR = "Incorrect Parameters\n";
const SYN_ERR = "Syntax Error\n";

if (!isset($argv[1]) || isset($argv[2])) {
    echo INC_PAR;
    exit();
}

if (!preg_match_all("/^\s*(?<num1>\d*)\s*(?<ope>\+|\-|\*|\/|\%)\s*(?<num2>(?1))\s*$/", $argv[1], $tab, PREG_SET_ORDER)) {
    echo SYN_ERR;
// exit(); ou else
} else {
    switch ($tab[0]['ope']) {
    case '+':
        echo $tab[0]['num1'] + $tab[0]['num2'] . "\n";
        break;

    case '-':
        echo $tab[0]['num1'] - $tab[0]['num2'] . "\n";
        break;

    case '*':
        echo $tab[0]['num1'] * $tab[0]['num2'] . "\n";
        break;

    case '/':
        echo($tab[0]['num2'] == 0 ? '0' : $tab[0]['num1'] / $tab[0]['num2']) . "\n";
        break;

    case '%':
        echo $tab[0]['num1'] % $tab[0]['num2'] . "\n";
        break;

    default:
        echo SYN_ERR;
    }
}
