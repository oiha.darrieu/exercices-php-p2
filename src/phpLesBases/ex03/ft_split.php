<?php

function ft_split(string $phrase)
{
    $tableau = preg_split("/\W/", $phrase, -1, PREG_SPLIT_NO_EMPTY);
    sort($tableau);

    return $tableau;
}

    // string = définit le type de caractères.
    // $phrase = paramètre qui n'est pas connu à l'avance. Ici une phrase dot on doit isoler les mots.
    // "/\W/" = REGEX qui définit les éléments séparateurs de notre chaîne.
    // -1 pour séparer TOUS les éléments de la chaîne.
    // sort($) pour classer par ordre alphabétique.
    // return pour renvoyer la valeur à l'EXTERIEUR de la fonction.
