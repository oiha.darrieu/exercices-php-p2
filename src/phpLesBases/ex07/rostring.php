<?php

if ($argc > 1) {// Ne traite que les arguments supérieurs à 1($value).
    $chaine = $argv[1]; // Isole et transforme le premier argument en chaîne.
    $tab = explode(' ', $chaine); // Transforme la chaîne en tableau.

    array_push($tab, $tab[0]); // !:array_push ne traite que les tableaux. On déclare d'abord la variable à traiter, puis l'élément qui va être DUPLIQUE A LA FIN, ici le premier élément de la chaîne.
    array_shift($tab); // Supprime le premmier élément de la chaîne. On déclare la chaîne à traiter. "!:".

    $string = implode(' ', $tab); // Transforme le tableau en chaîne.

    $sortie = preg_replace("/\s+/", ' ', $string); // On nettoie la chaîne pour qu'il n'y ai qu'un seul espace entre chaque mots.

    echo $sortie . "\n";
}

// AUTRE POSSIBILITE
/*
if ($argc >= 2) {

$tab = preg_split("/\s/", $argv[1], -1, PREG_SPLIT_NO_EMPTY);

$tab[] = array_shift($tab);

echo implode(' ', $tab)."\n";

}*/
