<?php

   $ar1 = array_slice($argv, 1); // affiche les paramètres à partir de la clé 1.
   $chaine = implode(' ', $ar1); // transforme la suite de param en chaine. Les espaces sont les éléments séparateurs. On a une chaine de mots.

   $mots = preg_split("/\s+/", $chaine, -1, PREG_SPLIT_NO_EMPTY); // on récupère tous les mots qui sont dans notre chaine. Le regex est l'élément séparateur, ici les espaces. Tout ce qui n'est pas un espace est récupéré.

   sort($mots); // tri les mots par ordre alphabétique.

   foreach ($mots as $key => $value) {
       echo $value . "\n";
   }
