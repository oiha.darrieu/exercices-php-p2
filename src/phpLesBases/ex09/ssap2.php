<?php

// $parametres = array_slice($argv, 1);//commence le tableau à partir du deuxième paramètre (clé 1).

$string = implode(' ', array_slice($argv, 1)); // conversion du tableau en chaîne pour utiliser preg_split.
$tab = preg_split("/\s+/", $string, -1, PREG_SPLIT_NO_EMPTY); // récupère tous les mots de la chaîne. !: Sors un tableau.

$alpha = [];
$num = [];
$caract = [];

foreach ($tab as $value) {// on utilise une boucle pour vérifier et trier chaque valeur du tableau.
    if (ctype_alpha($value) /* === true */) {// si la valeur ne contient que des caractères alphabétiques, la fonction est true.
        $alpha[] = $value; // on créer un tableau pour que la valeur une fois vérifiée soit répertoriée dans un groupe distinct. [] signifie que chaque valeur est ajoutée à la fin du tableau. ! : un tableau créé avec array() ne va pas cumuler les valeurs mais les écraser.
        // La variable doit être un tableau pour utiliser la fonction sort() et ses dérivés.
        // natcasesort($alpha);//on classe les caractères alphabétiques de façon insensitive.

        // var_dump($alpha); Pour tester une variable de tableau.
    } elseif (ctype_digit($value) /* === true */) {// si la valeur ne contient que des caractères numériques, la fonction est true.
        $num[] = $value;
    // sort($num, SORT_STRING);//SORT_STRING pour que les valeurs signées positives ou négatives soient classées.
    } else {
        $caract[] = $value;
        // sort($caract);
    }// tout le reste.
}

// trier en dehors de la boucle.
natcasesort($alpha);
sort($num, SORT_STRING);
sort($caract);

$resultats = [
    ...$alpha,
    ...$num,
    ...$caract,
];

foreach ($resultats as $resultat) {
    echo "$resultat\n";
}

// Création d'une boucle FOREACH pour afficher le contenu de chaque condition. FOREACH permet de traiter et d'afficher un tableau.
// foreach ($alpha as $key => $value1)//renommer $value pour ne pas confondre les groupes.
// {
//     echo $value1."\n";
// }

// foreach ($num as $key => $value2) {
//     echo $value2."\n";
// }

// if(isset($caract))//isset() permet au programme de continuer son exécution si la valeur est vide.
// {
//     foreach ($caract as $key => $value3) {
//         echo $value3."\n";
//     }
// }

// Notes :
// Pour récupérer plusieurs valeurs d'un tableau, utiliser FOREACH
// Pour récupérer une valeur, cibler avec $value = $tab[0]
