#Exercice 04 : aff_param
####Dossier de rendu : `ex04/`
####Fichiers à rendre : `aff_param.php`
####Fonctions Autorisées : toute la lib standard PHP
####Remarques : n/a

Très classique, ce programme affiche ses différents [paramètres de la ligne de commande](https://www.phpfacile.com/apprendre_le_php/parametres_d_entree_script_php), dans l’ordre reçu. Le nom du programme ne s’affiche pas.

```bash
$> php ./src/phpLesBases/ex04/aff_param.php
$> php ./src/phpLesBases/ex04/aff_param.php toto ahah foo bar quax
toto
ahah
foo
bar
quax
$>
```
