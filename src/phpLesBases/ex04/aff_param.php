<?php

// $argv : récupère les valeurs($value) sous forme de tableau
// $argc : récupère les clés($key) du tableau

// "print_r($argc/v);" : affiche les éléments sous forme d'array

// SOLUTION 1
/*for($i = 1; $i < $argc; $i++) {
     echo $argv[$i]."\n";
 }*/
 /*argc :
- Nous permet de traiter avec des chiffres.
- Utile s'il n'y a pas un nombre de valeurs prédéfinies.
- argc/argv peuvent coopérer.*/

// SOLUTION 2
foreach ($argv as $key => $value) {
    if ($key !== 0) {
        echo "$value\n";
    }
}
