<?php

/* CONSIGNE
Récupérer le premier paramètre.
Utiliser ce paramètre comme modèle de recherche de clé.

Pour chaque paramètre qui suit le premier paramètre :
Extraire le premier mot pour le transformer en clé.
Extraire le deuxième mot pour le transformer en valeur.

Parcourir le tableau clé/valeur avec le modèle pour récupérer la valeur correspondante.
[!] Si plusieurs valeurs avec la même clé, afficher la dernière valeur.
*/

// j'enferme le paramètre 2 dans une variable.
// $model_key = preg_match("/^\w+/", $argv[1]);

if (preg_match("/^\w+/", $argv[1], $match)) {
    $model_key = $match[0];
}

// je traite les autres paramètres à partir du 3eme
$tab = array_slice($argv, 2);

// je les transforme en chaine pour utiliser preg_split()
$str = implode(' ', $tab);

// je récupère les paramètres complets clé:valeur
if (preg_match_all("/\w+:\w+/", $str, $matches)) {
    $string = implode(' ', $matches[0]);

    // je récupère la valeur avant les ':'
    $cles = preg_split("/:\w*\s?/", $string, -1, PREG_SPLIT_NO_EMPTY);
    // :\w*\s? :\s?\w*|\s

    // je récupère la valeur après les ':'
    $valeurs = preg_split("/\s?\w*:/", $string, -1, PREG_SPLIT_NO_EMPTY);
    // \s?\w*: \s|\w*\s?:

    // je combine les tableaux pour que les valeurs de $cles soient converties en clés
    $tab_kv = array_combine($cles, $valeurs);

    foreach ($tab_kv as $key => $value) {
        if ($model_key == $key) {
            echo $value . "\n";
        }
    }
}
