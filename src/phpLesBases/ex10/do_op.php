<?php

if ($argc == 4) {
    $parametres = array_slice($argv, 1);

    $param[0] = trim($parametres[0]);
    $param[1] = trim($parametres[1]);
    $param[2] = trim($parametres[2]);

    if (is_numeric($param[0]) && is_numeric($param[2])) {
        if ($param[1] == '+') {
            echo $param[0] + $param[2] . "\n";
        }
        if ($param[1] == '-') {
            echo $param[0] - $param[2] . "\n";
        }
        if ($param[1] == '*') {
            echo $param[0] * $param[2] . "\n";
        }
        if ($param[1] == '/') {
            if ($param[2] == '0') {
                echo "0\n";
            } else {
                echo $param[0] / $param[2] . "\n";
            }
        }
        if ($param[1] == '%') {
            echo $param[0] % $param[2] . "\n";
        }
    } else {
        echo "Incorrect Parameters\n";
    }
} else {
    echo "Incorrect Parameters\n";
}
