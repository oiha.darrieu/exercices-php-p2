<?php
/*
si champs vide (null) ou identifiant existe déjà : ERROR\n
si tout est ok : OK\n

stocker les comptes créés dans un tableau serializé : /private/passwd

tab {
   compte 1 {
       [login] : password
   }
   compte 2 {
       [login] : password
   }
}

Vérifier que les données data_user sont bonnes.
Vérifier que la base de données existe. if

sérialiser

vérifier utilisateur dans la base de donnée avec une boucle.
s'il existe pas ajouter l'utilisateur au tableau.

Mettre à jour le contenu du fichier avec des données serializées.
Hasher les mots de passes.

*/

const ERROR = "ERROR\n";
const OK = "OK\n";

$directory = './private';
$passwd = 'passwd';
$login = $_GET['login'];
$pass = hash('sha256', $_GET['passwd']);
$tab_passwd = [];

// créer un tableau vide pour accueillir les data_user qui n'existent pas. Eviter de le mettre dans une condition.

mkdir($directory, 0777, false);

file_put_contents($directory . '/' . $passwd, 'data_user');

// if () {

// } else {
//     echo ERROR;
// }
