<?php

if ($argc != /* = */ 2) {
    exit();
}
// Si plus de 1 paramètre (hors nom du fichier), le programme ne s'exécutera pas.

$chaine = $argv[1]; // transforme le array $argv en chaine. [1] cible le premier paramètre (qui suit le nom du fichier) grâce à sa clé. Donc seul le paramètre [1] sera converti et traité.

$chaine = preg_replace("/\s+/", ' ', trim($chaine)); // Nettoyer la $chaine.
// preg_replace pour remplacer les éléments ciblés par d'autres éléments. Ici "/\s+/" tous les espaces blancs à un ou plusieurs caractères, par un seul espace.
// trim pour effacer les espaces en début et fin de la chaine appelée.

print_r($chaine . "\n"); // affiche le premier paramètre nettoyé.
