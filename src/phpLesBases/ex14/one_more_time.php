<?php

/*
________
CONSIGNE
Afficher le nombre de secondes écoulées depuis le 1er janvier 1970 en fonction d'une date donnée sous le format :
    lundi 1 Mars 2022 14 21 54 | Mercredi 13 mars 2022 02:00:23 (maj/min insensitive)

Pour tout autre format afficher :
    Wrong Format

_____________
DEVELOPPEMENT
Vérifier que le paramètre [1] est exploitable :
    - preg_match_all(REGEX) (pour vérifier les champs)
    - checkdate pour vérifier si la date existe (ne s'utilise qu'avec une date entièrement numérique. Convertir les mois en chiffres à l'aide d'un tableau)

Convertir la date française en anglais par rapport à l'UTC :
    - IntlDateFormatter::parse avec un FULL et la phrase $utc_paris

Calculer le nombre de secondes écoulées entre 1970 et la date données :
    - strtotime

________
ELEMENTS
$utc_paris = 'heure normale d’Europe centrale';

$regex = "/(lundi|mardi|mercredi|jeudi|vendredi|samedi|dimanche)\s(\d{1,2})\s(janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre)\s(\d{4})\s((([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9]))/i";

$fmt = new IntlDateFormatter(
    'de-DE',
    IntlDateFormatter::FULL,
    IntlDateFormatter::FULL,
    'America/Los_Angeles',
    IntlDateFormatter::GREGORIAN
);

echo 'Le premier format analysé est ' . $fmt->parse('Mittwoch, 20. Dezember 1989 16:00 Uhr GMT-08:00');
*/

$regex = "/(lundi|mardi|mercredi|jeudi|vendredi|samedi|dimanche)\s(\d{1,2})\s(janvier|février|mars|avril|mai|juin|juillet|août|septembre|octobre|novembre|décembre)\s(\d{4})\s((([0-1][0-9])|(2[0-3])):([0-5][0-9]):([0-5][0-9]))/i";

$tab_mois = ['janvier' => 1, 'février' => 2, 'mars' => 3, 'avril' => 4, 'mai' => 5, 'juin' => 6, 'juillet' => 7, 'août' => 8, 'septembre' => 9, 'octobre' => 10, 'novembre' => 11, 'décembre' => 12];

$utc_paris = 'heure normale d’Europe centrale';

const WR_FORM = "Wrong Format\n";

if (empty($argv[1])) {
    exit();
} elseif (preg_match_all($regex, $argv[1], $match)) {
    $case_mois = mb_convert_case($match[3][0], MB_CASE_LOWER, 'UTF-8');

    foreach ($tab_mois as $key => $value) {
        if ($case_mois == $key) {
            $mois = $value;
            break;
        }
    }

    if (checkdate($mois, $match[2][0], $match[4][0]) == true) {
        $date_to_tst = new IntlDateFormatter(
            'fr_FR',
            IntlDateFormatter::FULL,
            IntlDateFormatter::FULL,
            // 'America/Los_Angeles',
            // IntlDateFormatter::GREGORIAN
        );

        echo $date_to_tst->parse($argv[1] . $utc_paris) . "\n";
    } else {
        echo WR_FORM;
    }
} else {
    echo WR_FORM;
}
